import { createApp } from "vue";
import "./core/css/index.scss";
import App from "./app/App.vue";

createApp(App).mount("#app");
