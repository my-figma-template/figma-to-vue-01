import { render, screen } from "@testing-library/vue";
import HelloWorld from "./HelloWorld.vue";
describe("HelloWorld", () => {
	it("renders properly", async () => {
		const { getByTestId } = render(HelloWorld, {
			props: {
				name: "World",
			},
		});
		const element = getByTestId("your-component");
		expect(element).toBeInTheDocument();
		expect(element).toHaveTextContent("Hello World");
	});
});
