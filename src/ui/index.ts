export { default as NavBar } from "./NavBar.vue";
export { default as Header } from "./Header.vue";

export { default as SquareStat } from "./Statistiques/SquareStat.vue";
export { default as ChartStat } from "./Statistiques/ChartStat.vue";
