/// <reference types="vitest" />
/// <reference types="vite" />
import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";

// https://vitejs.dev/config/
export default defineConfig({
	plugins: [vue()],
	test: {
		globals: true,
		environment: "jsdom",
		css: true,
		setupFiles: "./src/test/setup.ts",
		coverage: {
			provider: "v8", // or 'istanbul'
		},
	},
});
